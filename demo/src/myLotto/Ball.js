import React from 'react'
import './Ball.css'
const Ball = (props) => {
    return (
        <div className="ball">
        <p className="oneBall">{props.num}</p>
    </div>
    );
};


export default Ball;