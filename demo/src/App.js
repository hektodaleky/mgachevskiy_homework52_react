import React, {Component} from "react";
import "./App.css";
import Ball from "./myLotto/Ball";


class App extends Component {
    state = {
        array: []
    };
    getRandomInt = (min, max) => {
        return Math.floor(Math.random() * (max - min + 1)) + min;

    };

    getNewBallNumber = () => {

        let innerArray;
        let tmp;
        if (this.state.array.length === 5)
            innerArray = [];
        else
            innerArray = this.state.array;

        while (true) {
            tmp = this.getRandomInt(5, 36);
            if (innerArray.indexOf(tmp) != -1)
                continue;
            break;
        }
        innerArray.push(tmp);
        innerArray.sort(function (a, b) {
            return a - b;
        });
        this.setState({
            array: innerArray

        })
    };

    render() {
        return (
            <div className="App">
                <div>
                    <button onClick={this.getNewBallNumber}>Get new Number</button>
                </div>
                <Ball num={this.state.array[0]}/>
                <Ball num={this.state.array[1]}/>
                <Ball num={this.state.array[2]}/>
                <Ball num={this.state.array[3]}/>
                <Ball num={this.state.array[4]}/>

            </div>
        );
    }
}

export default App;
